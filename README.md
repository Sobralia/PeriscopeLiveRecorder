# Periscope Live Recorder

### Version
2.1.1

NodeJs application that check for periscope profiles, save their lives and upload to YouTube channel.

### Required softwares
- NodeJs (obviously): https://nodejs.org/
- ffmpeg: https://ffmpeg.org/

### Installation
```sh
git clone git@git.framasoft.org:Sobralia/PeriscopeLiveRecorder.git
cd PeriscopeLiveRecorder
npm install
```

### Usage
```
node ./
```

Then open your web browser and go to this page: http://localhost:3000
to manage your profiles list.

# Changelog

## [2.1.0]
- Added license GNU GPLv3
- Create module to download hls chunks from periscope live
- Don't use livestreamer anymore
- Don't use ffmpeg to download video anymore
- Using ffmpeg only to rotate video
- Now if a periscope profile return 404, output the profile name in the console
- Changing video title by periscope title without hastags
- Adding date and time in youtube video description
- Adding original title to youtube video description
- Added checkbox on profile edit form to publish uploaded video or keep them private

### WARNINGS
- You'll probably have to create some sub directories in this version
- File credential.json is not included, you've to creat it

## [2.0.0]
- Recode database, recorder, upload modules
- Now use ffmpeg to download Video (later it will be only used to rotate and transcode)
- Enhanced web interface
- Create logger module to manage console and log files

## [1.2.1]
- Emergency bug fixes (updated broadcast status by filename instead of id atm)
- Disabling auto refresh list on web interface
- Do not start recording if the broadcast's id didn't change

## [1.2.0]
- Video can be automatically uploaded on YouTube

## [1.1.1]
- Bug fix: Status of lives don't wait for the recordBroadcast callback to toggle, they are updated on every check

## [1.1.0]
- Added unique ID for each video file saved to avoid file existing problem
- Colored console output
- Text field for YouTube description and hastags
- Live broadcast's infos are stored in the database for future auto upload on YouTube channel

## [1.0.0]
- web interface (http://localhost:3000) to manage periscope profiles without reboot the server
