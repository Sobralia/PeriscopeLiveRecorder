/*
  Periscope Live Recorder
  NodeJs application that check for periscope profiles, save their lives and upload to YouTube channel.

  Copyright (C) 2016 Olivier Cosquer olivier.cosquer@gmail.com

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require('app-module-path').addPath(__dirname + "/lib/");

var logger = require('logger');
var database = require('database');
var recorder = require('recorder');
var	web = require('web.js');
var	api = require('api.js');
var	upload = require('upload.js');
var express = require('express');
var app = express();
var router = express.Router();


logger.init();
database.reset_working_values();
recorder.init(upload);
api.init(router);
web.init(app, router, express);
upload.init(app);
