/*
  Periscope Live Recorder
  NodeJs application that check for periscope profiles, save their lives and upload to YouTube channel.

  Copyright (C) 2016 Olivier Cosquer olivier.cosquer@gmail.com

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

var fs = require('fs');
var Log = require('log');
var colors = require('colors');

module.exports = {
  log: null,
  read: null,
  filename: "",
  init: function(){
    var _this = this;
  	var date = new Date();
  	var	dateString = date.toLocaleDateString();
  	var	timeString = date.toLocaleTimeString();

    this.filename = "log/" + dateString + "-" + timeString.replace(/:/g, "-") + ".txt";
    this.log = new Log('debug', fs.createWriteStream(this.filename, {
      flags: 'a'
    }));
    this.info("Logger ready");
  },
  debug: function(line){
    console.log(colors.blue(line));
    this.log.debug(line);
  },
  error: function(line){
    console.log(colors.red(line));
    this.log.error(line);
  },
  info: function(line){
    console.log(colors.green(line));
    this.log.info(line);
  }
};
