var	periliveServices = angular.module("periliveServices", ["ngResource"]);

periliveServices.factory('Profile', ['$resource', function($resource){
    return $resource('/api/profiles/:_id', {
    	name: '@name',
    	id: '@_id',
    	islive: '@islive',
    	description: '@description',
        publish_videos: "@publish_videos",
    	hashtags: '@hashtags'
    }, {
    	update: {
    		method: 'PUT'
    	}
    });
  }]);
