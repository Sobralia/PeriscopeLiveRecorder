/*
  Periscope Live Recorder
  NodeJs application that check for periscope profiles, save their lives and upload to YouTube channel.

  Copyright (C) 2016 Olivier Cosquer olivier.cosquer@gmail.com

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

var database = require('database');
var logger = require('logger');

module.exports.init = function(router){
  /**
  * Rest API
  **/
  router.use(function(req, res, next) {
      next();
  });

  router.route('/profiles')
    .get(function(req, res){
      res.json(database.get_all_profiles())
    })
    .post(function(req, res){
      var new_profile = database.new_profile();

      new_profile.name = req.body.name;
      new_profile.description = req.body.description;
      new_profile.hashtags = req.body.hashtags;
      database.save_profile(new_profile);
      res.json({ message: req.body.name + ' profile created!' });
    });

  router.route('/profiles/:profile_id')
    .delete(function(req, res){
      database.delete_profile(req.params.profile_id);
      res.json({ message: req.params.profile_id + ' profile deleted!' });
    })
    .put(function(req, res){
      var profile = database.get_profile_by_id(req.params.profile_id);

      if (profile){
        profile.name = req.body.name;
        profile.description = req.body.description;
        profile.hashtags = req.body.hashtags;
        profile.publish_videos = req.body.publish_videos
        database.update_profile(profile);
        res.json({ message: req.params.profile_id + ' profile updated!' });
      }else{
        res.json({ message: req.params.profile_id + ' can\'t update profile' });
      }
    })
    .get(function(req, res){
      var profile = database.get_profile_by_id(req.params.profile_id);
      res.json(profile);
    });

    router.route('/profiles/:profile_id/broadcasts')
      .get(function(req, res){
        var profile = database.get_profile_by_id(req.params.profile_id);
        var response = {};

        if (!!profile && !!profile.broadcasts){
          response = profile.broadcasts;
        }
        res.json(response);
      });

    router.route('/broadcasts/')
      .get(function(req, res){
        var broadcasts_list = [];
        var profiles = database.get_all_profiles();
        var profiles_length = 0;
        var i = 0;

        if (!!profiles){
          while (i < profiles_length){
              broadcasts_list = broadcasts_list.concat(profiles.broadcasts);
            i++
          }
        }
        res.json(broadcasts_list);
      });

      router.route('/logs/')
        .get(function(req, res){

        });
    logger.info("API ready" .green);
};
