/*
  Periscope Live Recorder
  NodeJs application that check for periscope profiles, save their lives and upload to YouTube channel.

  Copyright (C) 2016 Olivier Cosquer olivier.cosquer@gmail.com

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

var fs = require('fs');
var request = require('request');
var exec = require('child_process').exec;
var shortid = require('shortid');
var events = require("events");
var util = require('util');

Downloader = function(params){
    var _this = this;
    this._construct = function(params){

    this.retry = 5;
    this.retry_left = 5;
    this.sequence = -1;
    this.chunks = [];
    this.event_emitter = new events.EventEmitter();

    this.url = params.url;
    this.path = params.path;
    this.output = params.output;
    this.chunk_base_url = params.url.substring(0, params.url.indexOf("playlist"));
    this.tmp_path = params.tmp_path;

    if (!fs.existsSync(params.path + "/tmp")){
      fs.mkdirSync(params.path + "/tmp");
    }if (!fs.existsSync(this.tmp_path)){
      fs.mkdirSync(this.tmp_path);
    }
    events.EventEmitter.call(this);
  };

  this.download_m3u8 = function(){
    request(this.url, function(err, response, body){
      if (err){
        console.log(err);
        _this.stop();
        return;
      }
      if (response.statusCode === 404 && _this.retry_left > 0){
        _this.retry_left--;
      }else if (response.statusCode === 200){
        var raw_data = body.split("\n");

        _this.retry_left = _this.retry;
        if (raw_data[0] === "#EXTM3U"){
          if (raw_data[3].indexOf("#EXT-X-MEDIA-SEQUENCE") > -1){
            _this.parse_m3u8(raw_data);
          }
        }
      }
      if (_this.retry_left == 0){
        _this.stop();
      }else{
        setTimeout(function(){
          _this.download_m3u8();
        }, 2000);
      }
    });
  };

  this.parse_m3u8 = function(raw_data){
    var i = 0;
    var length = raw_data.length;

    while (i < length){
      //Only lines that contains a chunk file
      if (raw_data[i].indexOf(".ts") > -1){
        _this.download_chunks(raw_data[i]);
      }
      i++;
    }
  };

  this.download_chunks = function(chunk_name){
    var filepath = _this.tmp_path + chunk_name;
    var url = _this.chunk_base_url + chunk_name;

    //Check if chunk was already downloaded
    if (_this.chunks.indexOf(chunk_name) == -1){
      var dest = fs.createWriteStream(filepath);
      _this.chunks.push(chunk_name);

      request.get(url)
      .on("error", function(err){
        console.log(err);
      })
      .pipe(dest);
    }
  };

  this.start = function(){
    this.download_m3u8();
  };

  this.create_chunks_list_file  = function() {
    var length = _this.chunks.length;
    var file = fs.createWriteStream(_this.tmp_path + "chunks_list.txt");

    for (var i = 0; i < length; i++){
      file.write("file '" + _this.chunks[i] + "'\n");
    }
    file.end();
  };

  this.concat_files = function(){
    var _this = this;
    var cmd = "ffmpeg -loglevel 8 -i " + _this.tmp_path +"raw.ts -vf transpose=dir=2 -bsf:a aac_adtstoasc -r 29.97 " + _this.output;

    exec("cat " + _this.tmp_path + "*.ts > " + _this.tmp_path + "raw.ts", function(err, stdout, stderr){
      if (err){
        console.log(err);
      }
      exec(cmd, function(err, stdout, stderr){
        if (err){
          console.log(err)
        }
        _this.clean();
      });
    });
  };

  this.stop = function(){
    _this.create_chunks_list_file();
    _this.concat_files();
  };

  this.clean = function(){
    this.emit("end");
  };

  return (this._construct(params));
};

util.inherits(Downloader, events.EventEmitter);
module.exports = Downloader;
