/*
  Periscope Live Recorder
  NodeJs application that check for periscope profiles, save their lives and upload to YouTube channel.

  Copyright (C) 2016 Olivier Cosquer olivier.cosquer@gmail.com

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

var db = require('diskdb');
var logger = require('logger');

db.connect("lib/db/", ["profiles"]);

module.exports = {
  reset: function(){
    db.profiles.remove();
    db.connect("lib/db/", ["profiles"]);
  },
  reset_working_values: function(){
    var profiles_list = this.get_all_profiles();
    var profiles_length = profiles_list.length;
    var profile;
    var i;

    for (i = 0; i < profiles_length; i++){
      profile = profiles_list[i];
      profile.last_broadcast_ids = [];
      profile.is_live = false;
      this.update_profile(profile);
    }
    logger.info("All working values are set to their default value");
  },
  new_profile: function(){
    return({
        name: "",
        is_live: false,
        last_broadcast_ids: [],
        description: "",
        publish_videos: false,
        rotate: {
          auto: false,
          angle: 90
        },
        hashtags: [],
        broadcasts: [],
        raw: null
    });
  },
  new_broadcast: function(){
    return ({
      filename: "",
      title: "",
      upload: {
        ready: false,
        done: false
      },
      raw: "",
    })
  },
  profile_exist: function(name){
    var profile = db.profiles.findOne({name: name});
    if (profile != null){
      return (false);
    }else{
      return (true);
    }
  },
  save_profile:  function(profile){
    db.profiles.save(profile);
  },
  update_profile:  function(profile){
    db.profiles.update({_id: profile._id}, profile);
  },
  delete_profile: function(profile_id){
    db.profiles.remove({_id: profile_id});
  },
  get_all_profiles: function(){
    return (db.profiles.find());
  },
  get_profile_by_id:  function(profile_id){
    var profile = db.profiles.findOne({_id: profile_id});

    if (profile !== null){
      return (profile);
    }
    return (null);
  },
  get_profile_by_name:  function(name){
    var profile = db.profiles.findOne({name: name});

    if (profile !== null){
      return (profile);
    }
    return (null);
  },
  get_broadcasts(profileId){
    var profile = this.get_profile_by_id(profileId);

    if (profile !== null){
      return (profile.broadcasts);
    }else{
      return (null);
    }
  },
  get_one_broadcast: function(broadcast_id){
    var profile = null;
    var broadcast = null;
    var profiles_list = this.get_all_profiles();
    var profiles_length = profiles_list.length;
    var i = 0;

    while (i < profiles_length && profile === null){
      var broadcasts_list = profiles_list[i].broadcasts;
      var broadcasts_length = broadcasts_list.length;
      var j = 0;

      while (j < broadcasts_length && broadcast == null){
        var tmp_broadcast = broadcasts_list[j];

        if (tmp_roadcast === broadcast_id){
          broadcast = tmp_broadcast;
        }
        j++;
      }
      i++;
    }
    return (broadcast);
  },
  exportDatabase: function(){
    return (this.getAllProfiles)
  },
  importDatabase: function(){
    //@TODO
  }
};
