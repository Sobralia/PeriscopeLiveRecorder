/*
  Periscope Live Recorder
  NodeJs application that check for periscope profiles, save their lives and upload to YouTube channel.

  Copyright (C) 2016 Olivier Cosquer olivier.cosquer@gmail.com

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

var database = require('database');
var logger = require('logger');
var	shortid = require('shortid');
var	fs = require('fs');
var	periscope = require('periscope-api');
var exec = require('child_process').exec;
var Downloader = require('hls_downloader');

module.exports = {
  upload: null,
  init: function(upload){
    this.upload = upload;
    this.check_profiles();
    logger.info("Recorder ready");
  },
  create_directories: function(){
    //Create Video and Database folder if not exist
    if (!fs.existsSync(__dirname + "/live")){
      fs.mkdirSync(__dirname + "/live");
    }if (!fs.existsSync(__dirname + "/db")){
      fs.mkdirSync(__dirname + "/db");
    }
  },
  create_profile_directory: function(profile_name){
    var	path = __dirname + "/live/" + profile_name;

    if (!fs.existsSync(path)){
      fs.mkdirSync(path);
    }
  },
  check_profiles: function(){
    var profiles_list = database.get_all_profiles();
    var profiles_length = profiles_list.length;
    var _this = this;

    for (var i = 0; i < profiles_length; i++){
      var the_profile = profiles_list[i];
      var name = the_profile.name;

      periscope.getProfileInfo(name, function(err, profile){
        if (err){
          if (profile !== "undefined"){
            logger.error("Error on profile: " + name)
          }
          logger.error(err);
          return;
        }
        if (profile !== "undefined" && _this.profile_is_live(profile)){
          logger.info(profile.info.username + " just started a live");
          _this.record_broadcast(profile);
        }
      });
    }

    setTimeout(function(){
      _this.check_profiles();
    }, 10000);
  },
  profile_is_live: function(profile){
    var is_live = false;
    if (!!profile && !!profile.info && !!profile.info.username){
      var db_profile = database.get_profile_by_name(profile.info.username);

      if (!!db_profile){
        var db_profile_broadcasts_length = db_profile.broadcasts.length;

        if (!!profile.lastBroadcast && profile.lastBroadcast.state === "RUNNING"){
          var aleady_recording = (db_profile.last_broadcast_ids.indexOf(profile.lastBroadcast.id) == -1) ? false : true;
          if (!aleady_recording){
            is_live = true;
            this.toggle_profile({name: profile.info.username}, true, profile.lastBroadcast.id);
          }
        }
      }
    }

    return (is_live);
  },
  get_filename: function(profile_name){
    var date = new Date();
    var	dateString = date.toLocaleDateString();
    var	timeString = date.toLocaleTimeString();
    var	path = __dirname + "/live/" + profile_name + "/"
    var	localfilename = profile_name + "_" + dateString + "_" + timeString.replace(/:/g, "-") + "_" + shortid.generate();

    var	filename = {
      path : path,
      localfilename: path + localfilename + ".mp4",
      title: profile_name + " " + dateString + " " + timeString
    }
    return (filename);
  },
  record_broadcast: function(profile){
    var filename = this.get_filename(profile.info.username);
    var _this = this;
    var broadcast_id = profile.lastBroadcast.id;
    var username = profile.info.username;

    this.create_profile_directory(username);
    logger.info("start recording: " + filename.localfilename);
    this.record_broadcast2(broadcast_id, filename, function(err, broadcast_info){
      var db_profile = database.get_profile_by_name(username);
      var new_broadcast = database.new_broadcast();

      if (err){
        logger.error(err);
      }else{
        new_broadcast.raw = broadcast_info;
        new_broadcast.filename = filename.localfilename;
        new_broadcast.title = filename.title;
        new_broadcast.upload.ready = true;
        db_profile.broadcasts.push(new_broadcast);
        database.update_profile(db_profile);

        //Delete broadcast from last_broadcast_ids
        _this.toggle_profile({name: username}, false, profile.lastBroadcast.id);
        logger.info(username + " ended live");
        logger.info("File saved: " + filename.localfilename);
        _this.upload.uploadVideos();
      }
    });
  },
  record_broadcast2: function(broadcast_id, filename, callback){
    periscope.getBroadcastInfo(broadcast_id, function(err, broadcasts_info){
      if (err){
        callback(err);
        return;
      }

      var params = {
          url: broadcasts_info.hls_url,
          path: filename.path,
          tmp_path: filename.path + "tmp/" + broadcast_id + "/",
          output: filename.localfilename
      };
      var recorder = new Downloader(params);

      recorder.start();
      logger.debug(broadcasts_info.hls_url);
      recorder.on("end", function(){
        callback(null, broadcasts_info);
      });
    })
  },
  toggle_profile: function(options, is_live, last_broadcast_id){
    var profile = null;

    if (options.id){
      profile = database.get_profile_by_id(options.id);
    }else if (options.name){
      profile = database.get_profile_by_name(options.name);
    }
    profile.is_live = is_live;
    if (is_live === false){
      var index = profile.last_broadcast_ids.indexOf(last_broadcast_id);
      profile.last_broadcast_ids.splice(index, 1);
    }else{
      profile.last_broadcast_ids.push(last_broadcast_id);
    }
    database.update_profile(profile);
  },
  get_youtube_name: function(broadcasts_info){

  }
}
