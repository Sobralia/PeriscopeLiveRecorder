/*
  Periscope Live Recorder
  NodeJs application that check for periscope profiles, save their lives and upload to YouTube channel.

  Copyright (C) 2016 Olivier Cosquer olivier.cosquer@gmail.com

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

var bodyParser = require('body-parser');
var logger = require('logger');

module.exports.init = function(app, router, express){
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());
  app.get('/', function (req, res) {
    var	options = {
    	root: __dirname + "/web/",
    	dotfiles: 'deny',
    	headers: {
    		"x-timestamp": Date.now(),
    		"x-sent": true
    	}
    };
    res.sendFile("index.html", options, function(err){
    	if (err) {
    	  logger.error(err);
    	  res.status(err.status).end();
    	}
    });
  });

  // Static routes for css, js, pictures, etc..
  app.use("/static", express.static("node_modules/bootstrap/dist"));
  app.use("/static/js", express.static("node_modules/angular"));
  app.use("/static/js", express.static("node_modules/angular-resource"));
  app.use("/static/js", express.static("node_modules/angular-route"));
  app.use("/static/js", express.static("node_modules/jquery/dist"));
  app.use("/static/js", express.static("lib/web/js"));
  app.use("/static", express.static("lib/web"));
  app.use("/partials", express.static("lib/web/partials"));
  // API route
  app.use('/api', router);

  // Launch server
  app.listen(3000, function () {
    logger.info('WebServer listening on port 3000!'.green);
  });
};
